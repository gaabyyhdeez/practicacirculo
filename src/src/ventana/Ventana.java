package src.ventana;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import src.dibujo.Dibujo;
import src.hilos.ConteoHilos;
import java.awt.Font;

public class Ventana{
	public static void main(String[] args)
	{	
		JFrame ventana = new JFrame("Prueba");
		
		JTextField txtHilos = new JTextField();
		JTextField txtPuntos = new JTextField();
		JTextField txtTiempo = new JTextField();
		
		Dibujo lienzo = new Dibujo();
		
		txtHilos.setBounds(500, 50, 50, 25);
		txtPuntos.setBounds(500, 100, 50, 25);
		txtTiempo.setBounds(500, 200, 50, 25);
		txtTiempo.setEnabled(false);
		
		ventana.getContentPane().add(txtHilos);
		ventana.getContentPane().add(txtPuntos);
		ventana.getContentPane().add(txtTiempo);
		
		ventana.getContentPane().add(lienzo);
		lienzo.setLayout(null);
		
		JLabel lblAproximacion = new JLabel("Aproximacion:");
		lblAproximacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAproximacion.setBounds(367, 156, 113, 16);
		lienzo.add(lblAproximacion);
		
		JLabel lblTiempo = new JLabel("Tiempo:");
		lblTiempo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTiempo.setBounds(410, 209, 70, 16);
		lienzo.add(lblTiempo);
		
		JLabel lblPuntos = new JLabel("Puntos:");
		lblPuntos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPuntos.setBounds(410, 104, 56, 16);
		lienzo.add(lblPuntos);
		
		JButton btnIniciar = new JButton("Generar puntos");
		btnIniciar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lienzo.add(btnIniciar);
		
		btnIniciar.setBounds(400, 276, 150, 33);
		btnIniciar.setEnabled(true);
		
		JLabel lblHilos = new JLabel("Hilos:");
		lblHilos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblHilos.setBounds(424, 52, 56, 16);
		lienzo.add(lblHilos);
		JTextField txtAproximacion = new JTextField();
		lienzo.add(txtAproximacion);
		txtAproximacion.setBounds(492, 153, 58, 25);
		
		txtAproximacion.setEnabled(false);
		
		btnIniciar.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						int noHilos, noPuntos;
						
						noHilos = Integer.parseInt(txtHilos.getText());
						noPuntos = Integer.parseInt(txtPuntos.getText());
						
						if(noHilos==1)
						{
							double origenX = 0.5;
							double origenY = 0.5;
							int conteoCirculo = 0;
							int contador = 0;
							
							long tiempoInicial = System.currentTimeMillis();
							
							do
							{	
								double x = Math.random();
								double y = Math.random();
								
								double distancia = Math.sqrt((Math.pow((origenX-x), 2)+Math.pow((origenY-y), 2)));
								
								int coordenadaX, coordenadaY;
								
								coordenadaX = (int) (25+(300*x));
								coordenadaY = (int) (25+(300*y));
								
								lienzo.paint(coordenadaX, coordenadaY);
								
								if(distancia<=0.5)
								{
									conteoCirculo++;
								}
								
								contador++;
							}while(contador < noPuntos);
							
							long tiempoFinal = System.currentTimeMillis();
							
							long tiempo = tiempoFinal-tiempoInicial;
							
							double aproximacion = 4*(conteoCirculo/((double)noPuntos));
							
							txtAproximacion.setText(String.valueOf(aproximacion));
							txtTiempo.setText(String.valueOf(tiempo));
						}
						else
						{
							int conteoCirculo = 0;
							int puntosHilo = noPuntos/noHilos;
							
							ConteoHilos tareas[] = new ConteoHilos[noHilos];
							
							for(int i=0; i<noHilos; i++)
							{
								tareas[i] = new ConteoHilos(puntosHilo, lienzo);
							}
							
							long tiempoInicial = System.currentTimeMillis();
							
							for(int i=0; i<noHilos; i++)
							{
								tareas[i].start();
							}
							for(int i=0; i<noHilos; i++)
							{
								try {
									tareas[i].join();
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
							for(int i=0; i<noHilos; i++)
							{
								conteoCirculo += tareas[i].getConteoHilos();
							}
							
							long tiempoFinal = System.currentTimeMillis();
							
							long tiempo = tiempoFinal-tiempoInicial;
							
							double aproximacion = 4*(conteoCirculo/((double)noPuntos));
							
							txtAproximacion.setText(String.valueOf(aproximacion));
							txtTiempo.setText(String.valueOf(tiempo));
						}
					}
				});
	
		ventana.setBackground(Color.WHITE);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setBounds(new Rectangle(0,0,620,400));
		ventana.setVisible(true);
	}
}
