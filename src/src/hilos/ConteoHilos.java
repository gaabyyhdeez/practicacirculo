package src.hilos;

import src.dibujo.Dibujo;

public class ConteoHilos extends Thread {
	int cantidadPuntos;
	int conteoHilo;
	Dibujo lienzo;
	
	public ConteoHilos(int numeroPuntos, Dibujo panel) 
	{
		cantidadPuntos = numeroPuntos;
		conteoHilo = 0;
		lienzo = panel;
	}
	
	public int getConteoHilos() 
	{
		return conteoHilo;
	}
	
	public void run() 
	{
		double origenX = 0.5;
		double origenY = 0.5;
		int contador = 0;
		
		do
		{
			double x = Math.random();
			double y = Math.random();
			
			double distancia = Math.sqrt((Math.pow((origenX-x), 2)+Math.pow((origenY-y), 2)));
			
			int coordenadaX, coordenadaY;
			
			coordenadaX = (int) (25+(300*x));
			coordenadaY = (int) (25+(300*y));
			
			lienzo.paint(coordenadaX, coordenadaY);
			
			if(distancia<=0.5)
			{
				conteoHilo++;
			}
			
			contador++;
		}while(contador < cantidadPuntos);
	}
	
	public static void main(String[] args) throws InterruptedException
	{
		
	}
}
