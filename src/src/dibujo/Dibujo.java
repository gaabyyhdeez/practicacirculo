package src.dibujo;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Dibujo extends JPanel{
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		this.setBackground(Color.white);
		
		g.setColor(Color.BLUE);
		g.drawRect(25, 25, 300, 300);
		g.drawOval(25, 25, 300, 300);
	}
	
	public void paint(int x, int y)
	{
		int origenX = 175;
		int origenY = 175;
		
		double distancia = Math.sqrt((Math.pow((origenX-x), 2)+Math.pow((origenY-y), 2)));
		if(distancia<=150)
			this.getGraphics().setColor(Color.GREEN);
		else
			this.getGraphics().setColor(Color.RED);
			
		this.getGraphics().drawLine(x, y, x, y);
	}
}
